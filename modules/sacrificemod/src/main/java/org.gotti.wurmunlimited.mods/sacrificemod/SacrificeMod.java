package org.gotti.wurmunlimited.mods.sacrificemod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.*;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

public class SacrificeMod implements WurmServerMod, PreInitable, Configurable {

	public static final String version = "ty1.2";
	public static final Logger logger = Logger.getLogger(SacrificeMod.class.getName());
	private static int boneChance = 100;

	private static final ArrayList<Integer> noBone = new ArrayList<>();
	private static final HashMap<Integer, Integer> lessChance = new HashMap<>();

	@Override
	public void configure(Properties properties) {
		boneChance = Integer.parseInt(properties.getProperty("boneChance", String.valueOf(boneChance)));
		for(String id : properties.getProperty("noBone", "").split(",")){
			if(!id.equals("")) {
				logger.info("NoBone entry: " + id);
				noBone.add(Integer.parseInt(id));
			}
		}
		for(String entry : properties.getProperty("lessChance", "").split(",")){
			if(!entry.equals("")) {
				logger.info("Less chance entry: " + entry);
				String[] keyValue = entry.split(";");
				lessChance.put(Integer.parseInt(keyValue[0]), Integer.parseInt(keyValue[1]));
			}
		}
		logger.info("boneChance: " + boneChance);
	}

	@Override
	public void preInit() {
		if (boneChance > 0) {
			try {
				CtClass[] paramTypes = {
						HookManager.getInstance().getClassPool().get("com.wurmonline.server.behaviours.Action"),
						HookManager.getInstance().getClassPool().get("com.wurmonline.server.creatures.Creature"),
						HookManager.getInstance().getClassPool().get("com.wurmonline.server.items.Item")
				};
				CtMethod sacrifice = HookManager.getInstance().getClassPool().getCtClass("com.wurmonline.server.behaviours.MethodsReligion").getDeclaredMethod("sacrifice", paramTypes);
				sacrifice.instrument(new ExprEditor() {
					boolean found = false;
					public void edit(MethodCall m)
							throws CannotCompileException {
						if (m.getClassName().equals("com.wurmonline.server.items.Item")
								&& m.getMethodName().equals("getRarity")) {
							if(found) {
								return;
							}
							StringBuilder statement = new StringBuilder("byte temp = $proceed($$);" +
								"$_ = temp;" +
								"if (temp > 0");
							for(int id : noBone){
								statement.append(" && items[x].getTemplateId() != ").append(id);
							}
							for(Map.Entry<Integer, Integer> entry : lessChance.entrySet()){
								statement.append(" && (items[x].getTemplateId() != ").append(entry.getKey())
									.append(" || com.wurmonline.server.Server.rand.nextInt(").append(entry.getValue()).append(") == 0)");
							}
							statement.append(
								") {"+
								"	if (com.wurmonline.server.Server.rand.nextInt(").append(boneChance).append(") == 0) {" +
								"	    final float ql2 = Math.max((com.wurmonline.server.Server.rand.nextFloat() * 100.0f), 1.0f);" +
								"	    com.wurmonline.server.items.Item bone;" +
								"		try {" +
								"			bone = com.wurmonline.server.items.ItemFactory.createItem(867, ql2, null);" +
								"	        bone.setRarity(temp);" +
								"	        performer.getInventory().insertItem(bone, true);" +
								"	        performer.getCommunicator().sendNormalServerMessage(altar.getBless().name + \" gives you a reward for your sacrifice.\");" +
								"		} catch (Exception e) {" +
								"			logger.log(java.util.logging.Level.SEVERE, \"Failed while creating bone from sacrifice: \", e);" +
								"		}" +
								"		$_ = 0;" +
								"	}" +
								"}");
							m.replace(statement.toString());
							found = true;
						}
					}
				});
			} catch (NotFoundException | CannotCompileException e) {
				throw new HookException(e);
			}
		}
		// arraylist and map no longer needed
		noBone.clear();
		lessChance.clear();
	}

	@Override
	public String getVersion(){
		return version;
	}
}
